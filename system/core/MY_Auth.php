<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 class MY_Auth extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->is_valid();
    }

    private function is_valid(){
        if(!$this->session->userdata('validated')){
            redirect('login');
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
 }
 ?>