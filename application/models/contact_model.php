<?php
    class Contact_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
        date_default_timezone_set('America/Los_Angeles');
	}
	
	function get_contacts($searchString='')
	{
        $searchString = $searchString ? strtolower($searchString) : '';
        $query = "SELECT `id`, `name`, `number`, `details`, `created`
                    FROM phone_book.contacts
                    WHERE LOWER(`name`) LIKE '%$searchString%'
                    OR LOWER(`number`) LIKE '%$searchString%'
                    OR LOWER(`details`) LIKE '%$searchString%'";
		$queryResults = $this->db->query($query);
        $results = $queryResults->result_array();
		return $results;
	}

    function get_contact($id)
	{
        $this->db->where('id', $id);
		$query_result = $this->db->get('contacts');
        if($query_result->num_rows() < 1)
        {
            return NULL;
        }
        $result = $query_result->result()[0];
        return array(
            'id'        => $result->id,
            'name'      => $result->name,
            'number'    => $result->number,
            'details'   => $result->details,
            'created'   => $result->created
        );
	}
	
	function store_contact($id, $name, $number, $details, $created)
	{
        $created = date('Y-m-d', strtotime($created));
        $contact = array(
            'name'      => $name,
            'number'    => $number,
            'details'   => $details,
            'created'   => $created
        );
        if(empty($id))
        {
            $this->db->insert('contacts', $contact);
        } else
        {
            $this->db->where('id', $id);
            $this->db->update('contacts', $contact);
        }
		return $this->db->affected_rows() > 0;
	}

    function remove_contact($id)
	{
        $this->db->where('id', $id);
        return $this->db->delete('contacts');
	}
}
?>