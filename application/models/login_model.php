<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function validate(){
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $query = $this->db->get('users');
        if($query->num_rows == 1)
        {
            // create session data
            $row = $query->row();
            $data = array(
                    'id' => $row->userid,
                    'first_name' => $row->first_name,
                    'last_name' => $row->last_name,
                    'username' => $row->username,
                    'validated' => true
                    );
            $this->session->set_userdata($data);
            return true;
        }
        // if no user then return false.
        return false;
    }
}
?>