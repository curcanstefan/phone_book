<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header <?php if(!empty($hide_search_bar)) echo "hidden"; ?>">
            <a class="navbar-brand" href="<?php echo base_url(); ?>contact/add">
                <span class="glyphicon glyphicon-plus"></span>
                Add new contact
            </a>
        </div>
    <div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="<?php if(!empty($hide_search_bar)) echo "hidden"; ?>">
            <div id="search-panel">
                <div id="search-inner-panel" class="input-group">
                    <input type="text" id="search-contact" class="form-control" placeholder="Search Contact">
                    <div class="input-group-btn" id="search-contact-btn">
                      <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </div>
        </li>
        <li class="<?php if(empty($hide_search_bar)) echo "hidden"; ?>">
            <a class="navbar-brand" href="<?php echo base_url(); ?>contactlist">Contacts</a>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a class="not-active" href="#">Hello <?php echo $user_first_name; ?></a></li>
        <li><a class="navbar-brand" href="<?php echo base_url(); ?>login/logout">Sign Out</a></li>
    </ul>
    </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>