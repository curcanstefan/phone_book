<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?= $title ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
    <link href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet"/>
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/util.js"></script>

	</head>
	<body>
		<div class="container">
			<?php echo $header; ?>
            <div id="content">
                <?php print $mainContent; ?>
            </div> <!-- end of content -->

			<?php echo $footer; ?>
		</div>

        <script type="text/javascript">
            var timeStarted = Date.now();
            var pageInitialized = false;
            console.log('html loaded at ' + Date.now());
            $(document).on('ready', function(){
                console.log('page ready at ' + Date.now());
                console.log('loaded in ' + (Date.now()-timeStarted)/1000 + ' seconds');
            });
        </script>
	</body>
</html>