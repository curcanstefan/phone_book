<!DOCTYPE html>
<html lang="en">
<head>
    <title>Phone book | Welcome</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
    <link href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet"/>
    <script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <h2>Hello</h2>
        <h2>please login to enter to your registered area</h2>
        <h2>&nbsp;</h2>
        <form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>login/process">
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="username" name="username" placeholder="username" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-4">
                    <input type="password" class="form-control" id="password" name="password" placeholder="password" value="">
                </div>
            </div>
            <?php if(!empty($error_message)) {
                    echo '<div class="alert alert-danger" role="alert">';
                    echo $error_message;
                    echo '</div>';
                }
            ?>
            <div class="form-group">
                <div class="col-sm-4 col-sm-offset-2">
                    <input id="submit" name="submit" type="submit" value="Login" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
</body>
</html>