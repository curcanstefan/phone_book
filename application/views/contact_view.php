<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>contact/<?php echo $action; ?>">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo $contact_name; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="phone-number" class="col-sm-2 control-label">Phone number</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="phone-number" name="phone-number" placeholder="0123456789" value="<?php echo $contact_number; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="details" class="col-sm-2 control-label">Additional notes</label>
        <div class="col-sm-4">
            <textarea class="form-control" rows="4" name="details"><?php echo $contact_details; ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="creation-date" class="col-sm-2 control-label">Date of adding</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="creation-date" name="creation-date" placeholder="" value="<?php echo $contact_creation; ?>">
        </div>
    </div>
    <input type="hidden" name="contact-id" value="<?php echo $contact_id; ?>">
    <?php if(!empty($error_message)) {
            echo '<div class="alert alert-danger" role="alert">';
            echo $error_message;
            echo '</div>';
        }
    ?>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
        </div>
    </div>
</form>

<script type="application/javascript">
    $(document).on('ready', function(){
        $('#creation-date').datepicker();
    });
</script>