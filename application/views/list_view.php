<script type="text/javascript">
    $(document).on('ready', function(){
        $('#search-contact').on('keyup', function() {
            var search_text = $('#search-contact').val();
            // call ajax to load just some contacts
            // or filter the existing contacts using JS
            var theTable = $('#contactsTable');
            searchValueInTable(theTable, this.value);
        });
    });
</script>

<div class="table-responsive">
    <table id="contactsTable" class="table table-hover">
        <thead>
            <tr>
                <td></td>
                <td>Name</td>
                <td>Number</td>
                <td>Details</td>
                <td>Creation Date</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($contacts as $contact) {
                echo '<tr>';
                echo '<td><a href="'.base_url().'contact/edit/'.$contact['id'].'" class="glyphicon glyphicon-pencil"></a></td>';
                echo '<td>'.$contact['name'].'</td>';
                echo '<td>'.$contact['number'].'</td>';
                echo '<td>'.$contact['details'].'</td>';
                echo '<td>'.$contact['created'].'</td>';
                echo '<td><a href="'.base_url().'contact/remove/'.$contact['id'].'" class="glyphicon glyphicon-remove"></a></td>';
                echo '</tr>';
            }
            ?>
        </tbody>
    </table>
</div>

<nav>
    <ul class="pagination"></ul>
</nav>