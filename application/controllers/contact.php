<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "system/core/MY_Auth.php";
class Contact extends MY_Auth
{
    function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->model('contact_model', '', TRUE);
	}
    
    private $contact_id    = '';
    private $name          = '';
    private $phone_number  = '';
    private $details       = '';
    private $creation_date = '';

    public function index()
    {
        $this->add();
    }

	public function add($contact_id='', $name='', $number='', $details='', $creation='', $error='', $add=TRUE)
	{
        // show add contact form
        $data_header = array(
            'hide_search_bar' => true,
            'user_first_name'=>$this->session->userdata('first_name')
            );
        $data_contact = array(
            'contact_id'        => $contact_id,
            'contact_name'      => $name,
            'contact_number'    => $number,
            'contact_details'   => $details,
            'contact_creation'  => $creation,
            'error_message'     => $error,
            'action'            => $add ? 'store' : 'do_remove'
            );
        $data = array(
			'title' => 'phone agenda',
            'header'         => $this->load->view('header_list', $data_header, TRUE),
			'footer'         => $this->load->view('footer', NULL, TRUE),
			'mainContent'    => $this->load->view('contact_view.php', $data_contact, TRUE),
            
		);
        $this->load->view('templates/default_template', $data);
	}

    public function store()
    {
        if(!$this->validate_input())
        {
            $errName = 'Please enter your name';
        }
        $success = $this->contact_model->store_contact(
            $this->contact_id,
            $this->name,
            $this->phone_number,
            $this->details,
            $this->creation_date
        );

        if($success) redirect('contactlist');
        // else show an error
        $this->add(
            $this->contact_id,
            $this->name,
            $this->phone_number,
            $this->details,
            $this->creation_date,
            'Please enter realistic values'
        );
    }

    private function validate_input()
    {
        $this->contact_id    = filter_input(INPUT_POST, 'contact-id',    FILTER_SANITIZE_STRING);
        $this->name          = filter_input(INPUT_POST, 'name',          FILTER_SANITIZE_STRING);
		$this->phone_number  = filter_input(INPUT_POST, 'phone-number',  FILTER_SANITIZE_STRING);
		$this->details       = filter_input(INPUT_POST, 'details',       FILTER_SANITIZE_STRING);
		$this->creation_date = filter_input(INPUT_POST, 'creation-date', FILTER_SANITIZE_STRING);
    }
    
    public function edit($id)
	{
        $contact = $this->contact_model->get_contact($id);
        if(empty($contact))
        {
            return $this->add('', '', '', '', '', 'Sorry, contact no longer exists');
        }

        $this->add(
            $contact['id'],
            $contact['name'],
            $contact['number'],
            $contact['details'],
            $contact['created']
        );
	}

    public function remove($id)
	{
        $contact = $this->contact_model->get_contact($id);
        if(empty($contact))
        {
            return $this->add('', '', '', '', '', 'Sorry, contact no longer exists');
        }

        $this->add(
            $contact['id'],
            $contact['name'],
            $contact['number'],
            $contact['details'],
            $contact['created'],
            'Are you sure you want to delete this contact ?',
            FALSE
        );
	}

    public function do_remove()
	{
        $this->validate_input();
        if($this->contact_model->remove_contact($this->contact_id))
        {
            redirect('contactlist');
        }
        $this->add(
            $contact['id'],
            $contact['name'],
            $contact['number'],
            $contact['details'],
            $contact['created'],
            'Sorry, we could not remove this contact',
            FALSE
        );
	}
}
?>