<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once "system/core/MY_Auth.php";
class ContactList extends MY_Auth
{
    function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->model('contact_model', '', TRUE);
	}

	public function index()
	{
        $contacts = array('contacts' => $this->contact_model->get_contacts());
        $data = array(
			'title' => 'phone agenda',
//			'active' => 'Defects Fixed',
			'header'			=> $this->load->view('header_list', array('user_first_name'=>$this->session->userdata('first_name')), TRUE),
			'footer'			=> $this->load->view('footer', NULL, TRUE),
			'mainContent'       => $this->load->view('list_view.php', $contacts, TRUE),
            
		);
        $this->load->view('templates/default_template', $data);
	}
}
?>