<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }

    public function index($msg = NULL){
        $data['msg'] = $msg;
        $this->load->view('login_view', $data);
    }

    public function process(){
        // load the model
        $this->load->model('login_model');
        // validate the user can login
        $result = $this->login_model->validate();
        if(! $result){
            $msg = '<font color=red>Invalid username and/or password.</font><br />';
            $this->index($msg);
        }else{
            // on success go to home page
            redirect('home');
        }        
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('login');
    }
}
?>