function searchValueInTable(table, filter) {
    filter = filter.toLowerCase();
    table = $(table).find('tbody');
    $(table).find('tr').each(function(){
        $(this).css('display','none');
        if($(this).find('td').text().toLowerCase().indexOf(filter) !== -1 ){
            $(this).css('display','table-row');
        }
    });
    showPageNumbers();
}

$(document).on('ready', function(){
    showPageNumbers();
});

var listElements = [];
var perPage = 10; 
var numItems = 0;
var numPages = Math.ceil(numItems/perPage);
var pagerClass = '.pagination';

function showPageNumbers() {
    listElements = $('#contactsTable tbody tr:visible');
    numItems = listElements.size();
    numPages = Math.ceil(numItems/perPage);
    
    $(pagerClass).attr("curr",0);

    var curr = 0;
    $(pagerClass+' li').remove();
    $('<li><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>').appendTo(pagerClass);
    while(curr < numPages){
      $('<li class="page_link"><a href="#">'+(curr+1)+'</a></li>').appendTo(pagerClass);
      curr++;
    }
    $('<li><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>').appendTo(pagerClass);

    $(pagerClass+' .page_link:first').addClass('active');

    listElements.css('display', 'none');
    listElements.slice(0, perPage).css('display', 'table-row');
    $(pagerClass+' li a').click(function(){
        var clickedPage = $(this).html().valueOf() - 1;
        if(typeof $(this).attr('aria-label') === 'undefined') goTo(clickedPage);
        if($(this).attr('aria-label') === 'Next') next();
        if($(this).attr('aria-label') === 'Previous') previous();
    });
}

function previous(){
    var goToPage = parseInt($(pagerClass).attr("curr")) - 1;
    if($('.active').prev('.page_link').length==true){
        goTo(goToPage);
    }
}

function next(){
    var goToPage = parseInt($(pagerClass).attr("curr")) + 1;
    if($('.active').next('.page_link').length==true){
        goTo(goToPage);
    }
}

function goTo(page){
    var startAt = page * perPage,
        endOn = startAt + perPage;

    listElements.css('display','none').slice(startAt, endOn).css('display','table-row');
    $(pagerClass).attr("curr",page);
    $(pagerClass+' .active').removeClass('active');
    $(pagerClass+' a').filter(function() { return ($(this).html() == page+1) }).parent().addClass('active');
}